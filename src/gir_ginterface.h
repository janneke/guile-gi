#ifndef _GIR_GINTERFACE_H
#define _GIR_GINTERFACE_H
#include <glib.h>

extern GQuark guginterface_type_key;
extern SCM GuGInterface_Type;

void gir_init_ginterface(void);
#endif
