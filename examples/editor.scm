;; Copyright (C) 2019  Michael L. Gran
;; Copyright (C) 2019  Jan (janneke) Nieuwenhuizen <janneke@gnu.org>

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https:;;www.gnu.org/licenses/>.
(use-modules (gi)
             (ice-9 receive)
             (srfi srfi-9))

(typelib-load "Gio" "2.0")
(typelib-load "Gdk" "3.0")
(typelib-load "Gtk" "3.0")
(typelib-load "GLib" "2.0")
(typelib-load "Pango" "1.0")

;; Oddly, the introspection information does not provide a constructor
;; for GtkTextIter.
(define (text-iter:new)
  (make-gstruct <GtkTextIter>))

(define (rgba:new)
  (make-gstruct <GdkRGBA>))

(define (print-goodbye widget)
  (display "Goodbye World\n"))

(define (key-press widget event)
  (receive (ok keyval)
      (with-object event (get-keyval))
      (format #t "key: ~s\n" keyval)
      #f))

(define-record-type <user-data>
  (make-user-data widget window view mode-line)
  user-data?
  (widget user-data-widget)
  (window user-data-window)             ; GI: avoid `children'
  (view user-data-view set-user-data-view)
  (mode-line user-data-mode-line))

(define (make-box children orientation)
  (warn 'make-box 'children children)
  (let ((box (create <GtkBox>
               (orientation (if (eq? orientation 'vertical)
                                ORIENTATION_VERTICAL
                                ORIENTATION_HORIZONTAL))
               (spacing 0))))
    (with-object box (set-homogeneous #t))
    (for-each
     (lambda (ui-data)
       (format (current-error-port) "ui-data ~a\n" ui-data)
       (let* ((w (if (user-data? ui-data) (user-data-widget ui-data) ui-data))
              (parent (with-object w (get-parent))))
         (format (current-error-port) "w= ~a\n" w)
         (when w
           (when parent
             (let ((parent (cast parent <GtkBox>)))
               (with-object parent (remove w))))
           (with-object box (pack-start w #t #t 0)))))
     children)
    (with-object box (show-all))
    box))

(define (make-buffer-window)
  (let ((vbox (create <GtkBox>
                (orientation ORIENTATION_VERTICAL)
                (spacing 0)))
        (ebox (create <GtkEventBox>))
        (bgc (cast (rgba:new) <GdkRGBA>))
        (mode-line (create <GtkLabel>))
        (window (create <GtkScrolledWindow>))
        (view (create <GtkTextView>)))

    (using mode-line
      (set-halign ALIGN_START)
      (set-markup " :--- guimax")
      (set-line-wrap #f))
    (using window (set-policy POLICY_AUTOMATIC POLICY_AUTOMATIC))
    (using bgc (parse? "lightgray"))
    (using ebox (override-background-color STATE_NORMAL bgc))

    (using view (modify-font (font-description-from-string "monospace 18")))

    (using vbox
      (pack-start window #t #t 0)
      (pack-start ebox #f #f 0))
    (using ebox (add mode-line))
    (using window (add view))

    (format (current-error-port) "buffer-window ~a\n" vbox)
    (make-user-data vbox window view mode-line)))

(define (activate app)
  (let* ((window (cast (application-window:new app) <GtkApplicationWindow>))
         (vbox (cast (vbox:new 0 0) <GtkVBox>))
         (button-box (cast (button-box:new 0) <GtkButtonBox>))
         (button-hello (button:new-with-label "Hello"))
         (button-split (button:new-with-label "Split"))
         (button-quit (button:new-with-label "Quit"))
         (buffer-window (make-buffer-window))
         (editor (user-data-view buffer-window))
         (container (user-data-widget buffer-window)))
    (with-object editor
      (add-events EVENT_MASK_KEY_PRESS_MASK)
      (connect! key-press-event key-press))
    (with-object button-box
      (add button-hello)
      (add button-split)
      (add button-quit))
    (with-object vbox
      (add container)
      (add button-box))
    (with-object window
      (set-title "Window")
      (set-default-size 200 200)
      (add vbox))

    (with-object button-quit
      (connect! clicked print-goodbye)
      (connect! clicked (lambda x
                         (with-object window (destroy)))))

    ;; When the 'hello' button is clicked, write the current contents
    ;; of the editor to the console, and replace the buffer contents
    ;; with 'Hello, world'.
    (with-object button-hello
      (connect! clicked (lambda x
                         (let ((buffer (with-object editor (get-buffer)))
                               (iter1 (text-iter:new))
                               (iter2 (text-iter:new)))
                           (with-object buffer (get-bounds iter1 iter2))
                           (let ((txt (with-object buffer (get-text iter1 iter2 #t))))
                             (write txt) (newline))
                           (with-object buffer (set-text "Hello, world" 12))))))

    (with-object button-split
      (connect! clicked (lambda x
                          (let* ((children (list buffer-window (make-buffer-window)))
                                 (container' (make-box children 'horizontal)))
                            (with-object vbox
                              (remove container)
                              (add container')
                              (show-all))))))

    (with-object editor (grab-focus))
    (with-object window (show-all))))

(define (main)
  (with-object (application:new "org.gtk.example" 0)
    (connect! activate activate)
    (run (length (command-line)) (command-line))))

(main)
