#+TITLE: Hacking this Guile GObject Introspection Library
#+AUTHOR: Michael Gran

* Header Files with no dependencies

pygi-argument.h
pygi-foreign-api.h
pygi-invoke-state-struct.h
pygi-struct.h
pygi-value.h
pygobject.h
pygoptiongroup.h
pygparamspec.h
pygpointer.h


* pygobject as a standalone library

  pygobject-object.c

* The GObject Wrapper Class

  

* Python Types
  
  The PyTypeObject (which holds the information about a Python
  foreign-object type) and the Guile foreign-object type serve the
  same purpose.  They allow the host language to define a type that
  wraps an arbitrary C or native language type.  But their approaches
  are quite different.

  The Guile foreign-object type is just
  - the name of the type
  - the count and names of the slots
  - and an optional finalizer procedure.

  The slots on a Guile foreign-object type are just boxes; they can
  contain anything.

  Python types store a trememdous amount of information about the
  types in the types themselves, whilst in Guile foreign-object type,
  the metadata is limited.

  Python types contain references to 

  - tp_name: a pointer to a NUL-terminated string containing the name of
    the type
  - tp_dealloc: a pointer to the instance destructor function, which
    is called if the reference count reaches zero
  - tp_flags:
    + Py_TPFLAGS_HEAPTYPE: the type object itself is allocated on the
      heap
    + Py_TPFLAGS_BASETYPE: the type can be used as the base type of
      another type
    + Py_TPFLAGS_HAVE_GC: the type supported garbage collection
  - tp_doc: a pointer to a NUL-terminated C string giving the
    docstring for this stype object
  - tp_dict: a dictionary containing initial attributes for the type.
  - tp_getattro: a procedure to get attributes for the type.
  - tp_setattro: a procedure to set attributes for the type.

  and many, many more.

  The pragmatic part of this is that a Python class type has enough
  introspection information that, given a PyTypeObject, one can
  completely create and destroy instances, including the instantiation
  of any Python objects, C pointers, variables, or structs it may
  contain.  A Guile foreign-object class instance only contains named
  slots.  It requires an additional procedure to populate those slots
  with valid data.

  To mock up PyGObject, will we need to add additional information to
  Guile foreign-object types so that a type has enough information to
  create an instance and populate it with default values?  It will
  need a refcnt slot as explained below, as well as a finalizer that
  can be run multiple times on the same object.

  GuGObject slots
  - ob_type: holds the type of which this class is an instance

* Instances of Python Types

  Instances of Python types have an refcnt, initialized to 1 when the
  instance is constructed.  When the refcount reaches zero, the type's
  destructor is called.

  To mock up instances of Python Types in Guile, the Guile
  foreign-object types will need to have a refcnt slot, which is
  initizalized to 1.  And they will need to have a destructor that can
  be run multiple times, e.g. when the refcnt reaches zero as well as
  when the instance is garbage collected.

  Slots
  - SCM ob_type: The Guile foreign object type of which this object is an instance
  - SCM ob_refcnt: The reference count of this object
  - type: The GType of the contents
  - obj: A pointer to the (probably GObject *) contents
  - inst_dict: Another mostly unused hash table
  - weakreflist: an mostly unused weak-vector
  - flags: the GuGObjectFlags

  ** Dictionaries and attributes
  There are a lot of dictionaries in Python types: tp_dict, tp_dictoffset, inst_dict.
  As near as I can tell, when you use the GetAttr type functions, we want to
  - First, look it up in the Class's tp_dict
  - If not found, look it up in the instance's inst_dict
  Not sure what happens if an object is a type

  But, SetAttr type functions of an instance only modify the inst_dict, and 
  can't override anything defined in the ty_dict of the type.

* PyGObject Types

  PyGObject Types are Python types that hold a GObject pointer that
  itself holds an instance of a GObject-based class.  There is a
  Python class instance envelope about a GObject class instance.

  Now, The GObject class itself holds a set of properties 
  and signals.

  From PyGObject, the C structure that will form the contents of a
  PyGObject type will have a PyObject_HEAD entry.  Replace
  PyObject_HEAD with ssize_t ob_refcnt and SCM ob_type.  ob

  PyObject_HEAD
  -->
  ssize ob_refcnt;
  SCM ob_type;      // holds the Guile type of this type

* Instances of PyGObject Types

  In addition to the refcnt that is required for all instances of
  PyObject types, instances of PyGObject types also have slot that
  contains a 
  - a GObject pointer
  - an instance dictionary, which is initially set to the contents of
    the PyGObject Type's default dictionary
  - a weak ref list
  - some GObject Flags

** Glib GList -> Guile list
** Glib GSList -> Guile list

** Python Weak Ref List -> Guile weak-vector
   There is a Python weak ref list in PyGObject, but,
   is isn't used in PyGObject itself.

** Python Dictionaries -> Guile hash tables

** C PyObject * -> regular SCM of any type
   Usually a pointer to a PyObject on the heap.

** C PyTypeObject * ->

** C struct _PyGClosure -> C struct _GuGClosure
   - GClosure closure: ??
   - PyObject *callback -> SCM callback - a procedure of ??
   - PyObject *extra_args -> SCM extra_args: an SCM list
   - PyObject *swap_data -> SCM swap_data: ??
   - GuClosureExceptionHandler exception handler: ??

** C PyObject_HEAD > c struct fragment
   - ssize_t ob_refcount
   - PyTypeObject *ob_type -> SCM ob_type

** C PyGObject * -> C GuGObject * struct
   Slots
   - ssize_t ob_refcount
   - PyTypeObject *ob_type
   1. GObject *obj: a C pointer
   2. SCM inst_dict: SCM containing a Guile hash-table
   3. SCM weakreflist: SCM containing a weak-vector
   4. GuGObjectFlags flags: a C enum

** C struct PyGBoxed -> C struct GuGBoxed;
   Slots
   - ssize_t ob_refcount
   - PyTypeObject *ob_type
   1. gpointer boxed: a C pointer to any type of data that has a GType ID
   2. GType gtype: the GType ID of the data pointed to by 'boxed'
   3. gboolean free_on_dealloc: a C boolean

** Python 'Boxed' -> Guile foreign object type 'Boxed'

** PyGPointer -> a foreign object type
   Slots
   - ssize_t ob_refcount
   - PyTypeObject *ob_type
   1. gpointer pointer: a C pointer to any type of data with a GType ID
   2. GType gtype: the GType ID of the data in 'pointer'

** C struct PyGParamSpec -> C struct GuGParamSpec
   - ssize_t ob_refcount
   - PyTypeObject *ob_type

   - GParamSpec *pspec;

** Python ParamSpec
* Hacking this Guile GObject Introspection library


* Specific functions

PyTuple_Size(tuple)
-> scm_to_ssize_t (scm_length (list))

PyArgs_ParseTuple
-> GuArg_ParseList

PyTuple_GetItem
-> scm_list_ref (list, scm_from_ssize_t (x))

PYGLIB_PyLong_Check
-> scm_is_exact_integer

pygi_gint_from_py
scm_to_int
  
** Types are complicated

*** Making a GType

Mostly, we're not making any GTypes.  We are using existing GObject
types.

The functionality to make a new GType goes like this.

We use the g_type_register_static() procedure to make a new type.
Since we're not doing any abstract classes or anything like that, we
just need
- a parent GType ID
- a type name
- a GClassInitFunc class_init function, where you set properties,
  callbacks, an signals for the class
- a GInstanceInitFunc instance_init function, where you set reasonable
  default values


In the class_init_function, we override the class->set_property and
class->get_property procedures to the shims gir_object_set_property
and gir_object_get_property.  These shims then call the Guile
procedures of the form (do-set-property spec value) and (do-get-property spec).

Also in the class_init function, we take a hash table of signals
to create or overrride.

Also in the class_init function, we take a hash table of properties
to install.

We end up wrapping a C GType as Guile GOOPS class.
The GOOPS class has these slots
- __name__: the name of the GType
- __init__: a procedure called on a newly constructed GObject of this type
- __gtype__: the C GType
- __doc__: maybe a docstring?
- __gproperties__: the hash table of properties
- __gsignals__: 
- do_set_property: a procedure to set a property
- do_get_property: a procedure to get a property

Then, so that we have access to the GOOPS class from C, we stuff a
reference to the GOOPS class in the QData of the GType

In the instance_init function, we install the do-set-property and 
do-get-property wrapper in the QData of instance.  Then we call
the init procedure on the instance.

In the instance_init function, 


3. a GTypeInfo structure

This last one 
GIR follows a strategy used in PyGObject for expressing GObject types
as Guile types.

Innermost, you have a GObject type.

First call to g_object_new
1. It runs the class_init function.  The class init should initialize
   or override class methods (that is, assign to each class' method
   its function pointer) and to create the signals and properties
   associated to your object.

Each call to g_object_new
1. It calls the target type's class constructor method.  Usually you
   don't do anything here.
2. It calls the instance_init function.  Each instance_init is called
   from the base class up to this class.  This is the standard
   constructor.
3. The target type's constructed method is where you would handle
   post-construction duties.  Probably don't to anything here, either.

Each call to g_object_unref
1. the target type's dispose function should release any references to
   other objects.  Dispose can be executed more than once.  Dispose
   should chain up to its parent implementation.  Also, any functions
   registered as g_object_weak_ref, or pointers registered by
   g_object_add_weak_pointer are removed here.
2. The target type's finalize completes destruction by freeing the 
   last of self's objects


- GType: a numerical value which represents the unique identifier of a
  registered type.
- class_init: initializers
- base_finalize and class_finalize: destructors
- copy functions
- type characteristics

GObjects have their own memory management, based on reference
counting.  Do g_object_unref to properly free an object.

g_object_weak_ref add a callback that is called during 'dispose'

The 'dispose' is when the gobject releases all references to other
member objects.  Dispose may be called multiple times.

The 'finalize' is when the gobject completes its own destruction.

g_object_unref kicks of both dispose and finalize.

* Prospective R5RS-like API

A type definition
("name"
 GType-of-parent
 ((name nick blurb min max default flags)
  (name nick blurb min max default flags))
 ((name returntype (paramtype paramtype) accum data)
  (name returntype (paramtype paramtype) accum data))
 finalizer)

(string->gtype "gint")

struct _GiObject
{
  <ParentType> parent which is GObject + parent params
  <param-store> -> #('typename GType pointer-to-GiObject offset-to-start-of-param-store
                      refcnt finalizer param param param ...)
}

The GObjectGetPropertyFunc get a property-id so you can index into the param store to
get the param

(make-gobject GType [param1] [param2] [param3] ...)

  returns a vector #('typename GType pointer offset refcnt finalizer param param param)

I guess properly constructed gobject has a non-zero pointer, offset

Maybe if you made such a vector by hand like #('gobject typename gtype NULL NULL finalizer param param param)
you could have a procedure like
(vector->gobject vector) which then returns a new vector with the pointer and offset filled in

(set-property-value! <gobj> "name" value)
(get-property-value <gobj> "name")  ; usually copies the thing

Params could be boxed types, or variants
